package com.project.middleware.controller;

import com.project.middleware.model.request.UserRequest;
import com.project.middleware.service.MainService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class MainController {

    private final MainService mainService;

    @GetMapping(value = "/admin")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> admin() {
        return ResponseEntity.ok("admin");
    }

    @GetMapping(value = "/client")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
    public ResponseEntity<?> client() {
        return ResponseEntity.ok("client");
    }

    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@RequestBody UserRequest userRequest) {
        String login = mainService.signin(userRequest);
        return ResponseEntity.ok(login);
    }
}

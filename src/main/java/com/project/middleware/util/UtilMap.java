package com.project.middleware.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class UtilMap {

    public static String convertObjectToStringJson(Object obj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        return (mapper.writeValueAsString(obj)) != null ?
                (mapper.writeValueAsString(obj)).replaceAll("\\s+", " ")
                : null;
    }
}

package com.project.middleware.config;

import com.project.middleware.service.HttpServletReqUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Optional;

@Slf4j
@Component
public class MyInterceptor implements HandlerInterceptor {

    @Autowired
    private HttpServletReqUtil reqUtil;

    private String[] listOfSqlInjection = {
            "--", "//", "\\", ";",
            "ORDER BY", "GROUP BY", "UNION SELECT",
            "sleep(", "waitfor ", "benchmark(", "SELECT *"
    };

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) {

        final MyHttpServletRequestWrapper wrappedRequest = new MyHttpServletRequestWrapper(request);

        /*System.out.println("Pre handle method has been called");
        System.out.println("User IP address: " + reqUtil.getRemoteAddress(wrappedRequest));
        System.out.println("Http request: " + request.getMethod().toUpperCase());*/

        if ("GET".equalsIgnoreCase(request.getMethod())) {
            System.out.println("Request Params: " + reqUtil.getRequestParams(wrappedRequest));
            validateRequestValues(reqUtil.getRequestParams(wrappedRequest));
        } else {
            System.out.println("Request Payload: " + reqUtil.getPayLoad(wrappedRequest));
            validateRequestValues(reqUtil.getPayLoad(wrappedRequest));
        }

        /*System.out.println("Exiting Pre handle method");*/
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        //
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        //
    }

    private void validateRequestValues(String reqUtil) {
        Optional<String> optional = Arrays.stream(listOfSqlInjection).filter(s -> StringUtils.containsIgnoreCase(reqUtil, s)).findFirst();
        if (optional.isPresent()) {
            throw new RuntimeException("blacklist is not blocked: " + optional.get());
        } else {
            System.out.println("blacklist is not blocked");
        }
    }
}

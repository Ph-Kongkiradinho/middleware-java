package com.project.middleware.config;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
@Order(1)
public class TransactionFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final MyHttpServletRequestWrapper wrappedRequest = new MyHttpServletRequestWrapper(
                (HttpServletRequest) request);
        chain.doFilter(wrappedRequest, response);
    }
}
